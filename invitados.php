<?php include_once './includes/templates/header.php'; ?>

<?php        
    try {
        // Conexión
        require_once('includes/functions/bd_conexion.php');
        // Consulta
        $sql = "SELECT * FROM invitados ";
        // Consultar la BD
        $resultado = $conn->query($sql);
    } catch (Exception $e) {
        $error = $e->getMessage();
    }
?>

    <section class="invitados seccion contenedor">
        <h2>Nuestros Invitados</h2>
        <ul class="lista-invitados clearfix">
            <?php while($invitados = $resultado->fetch_assoc() ) { ?>
                
                <li>
                    <div class="invitado">
                        <a class="invitado-info" href="#invitado<?php echo $invitados['invitado_id']; ?>">
                            <img src="img/<?php echo $invitados['url_imagen']; ?>" alt="Imagen Invitado">
                            <p>
                                <?php echo $invitados['nombre_invitado'] . " " . $invitados['apellido_invitado']; ?>
                            </p>
                        </a>
                    </div>
                </li><!-- Invitado 1 -->

                <div style="display:none;">
                    <div class="invitado-info" id="invitado<?php echo $invitados['invitado_id']; ?>">
                        <h2><?php echo $invitados['nombre_invitado'] . " " . $invitados['apellido_invitado']; ?></h2>                  <img src="img/<?php echo $invitados['url_imagen']; ?>" alt="Imagen Invitado">
                        <p><?php echo $invitados['descripcion']; ?></p>  
                    </div>
                </div>

            <?php } //While del fetch_assoc ?>
        </ul>
    </section>

    <!-- Cerrar la Conexión -->
    <?php $conn->close(); ?>

<?php include_once './includes/templates/footer.php'; ?>  