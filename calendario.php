<?php include_once './includes/templates/header.php'; ?>
   
    <section class="seccion contenedor">
        <h2>Calendario de Eventos</h2>

        <?php 
        
            try {
                // Conexión
                require_once('includes/functions/bd_conexion.php');
                // Consulta
                $sql = " SELECT evento_id, nombre_evento, fecha_evento, hora_evento, cat_evento, icono, nombre_invitado, apellido_invitado ";
                $sql .= " FROM eventos ";
                $sql .= " INNER JOIN categoria_evento ";
                $sql .= " ON eventos.id_cat_evento = categoria_evento.id_categoria ";
                $sql .= " INNER JOIN invitados ";
                $sql .= " ON eventos.id_inv = invitados.invitado_id ";
                // Consultar la BD
                $resultado = $conn->query($sql);
            } catch (\Exception $e) {
                echo $e->getMessage();
            }
        
        ?>
        
        <div class="calendario">
            <div>
                <?php 
                    $calendario = array();
                    // Imprimir Resultados
                    while( $eventos = $resultado->fetch_assoc()){

                        $fecha = $eventos['fecha_evento'];
                        
                        $evento = array(
                            'titulo' => $eventos['nombre_evento'],
                            'fecha' => $eventos['fecha_evento'],
                            'hora' => $eventos['hora_evento'],
                            'categoria' => $eventos['cat_evento'],
                            'icono' => $eventos['icono'],
                            'invitado' => $eventos['nombre_invitado'] . " " . $eventos['apellido_invitado']
                        );

                        $calendario[$fecha][] = $evento;

                    ?>

                <?php } //While del fetch_assoc ?>



                <?php 
                    //Imprime todos los eventos
                    foreach($calendario as $dia => $lista_eventos){ ?> 
                    <h3>
                        <i class="far fa-calendar-alt"></i>
                        <?php 
                            // Linux
                            setlocale(LC_TIME, 'es_ES.UTF-8');
                            // Windows
                            setlocale(LC_TIME, 'spanish');

                            echo strftime("%A, %d de %B del %Y",strtotime($dia));
                        ?>
                    </h3>

                    <?php foreach($lista_eventos as $evento){  ?>
                        <div class="dia">
                            <p class="titulo">
                                <?php echo $evento['titulo']; ?>
                            </p>
                            <p class="hora">
                                <i class="far fa-clock"></i>
                                <?php echo $evento['fecha']." / ".$evento['hora']; ?>
                            </p>
                            <p class="categoria">
                                <i class="fa <?php echo $evento['icono'] ?>"></i> 
                                <?php echo $evento['categoria']; ?>
                            </p>
                            <p class="invitado">
                                <i class="fas fa-user"></i>
                                <?php echo $evento['invitado']; ?>
                            </p>
                        </div>

                    <?php } ?> <!-- Cierre foreach eventos -->
                <?php } ?> <!-- Cierre foreach dia (fecha) -->

                
            </div>

    <!-- Cerrar la Conexión -->
            <?php $conn->close(); ?>
        </div><!-- Cierre Calendario -->
    </section>


<?php include_once './includes/templates/footer.php'; ?>  